﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LinqToDB.Mapping;

namespace AddressBookWebTests
{
    [Table(Name = "group_list")]
    public class GroupData : IEquatable<GroupData>, IComparable<GroupData>
    {
        public GroupData()
        {
        }
        public GroupData(string pName)
        {
            GroupName = pName;
        }

        // Overloaded, used in "Assert.AreEquals()"
        public bool Equals(GroupData other)
        {
            if (object.ReferenceEquals (other, null))
            {
                return false;   // GroupData is null
            }
            if (object.ReferenceEquals(this, other))
            {
                return true;    // GroupData is the same
            }

            return GroupName == other.GroupName;    
        }

        public int CompareTo(GroupData other)
        {
            if (object.ReferenceEquals(other, null))
            {
                return 1;   // Current object is greater..
            }

            return GroupName.CompareTo(other.GroupName);
        }

        // Overloaded, used in "Assert.AreEquals()"
        public override int GetHashCode()
        {
            return GroupName.GetHashCode();
        }

        public override string ToString()
        {
            return "Group name = " + GroupName + "\nGroup header = " + GroupHeader
                + "\nGroup footer = " + GroupFooter;
        }

        [Column(Name = "group_name")]
        public string GroupName {get; set;}

        [Column(Name = "group_header")]
        public string GroupHeader {get; set;}

        [Column(Name = "group_footer")]
        public string GroupFooter {get; set;}

        [Column(Name = "group_id"), PrimaryKey, Identity]
        public string Id {get; set;}

        public static List<GroupData> GetAllGroupsFromDB()
        {
            AddressBookDb lDb = new AddressBookDb();
            List<GroupData> lGroupsFromDb = (
                from groups in lDb.Groups
                select groups).ToList();
            lDb.Close();
            return lGroupsFromDb;
        }
    }
}