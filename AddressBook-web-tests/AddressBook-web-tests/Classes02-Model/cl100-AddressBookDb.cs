﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LinqToDB;

namespace AddressBookWebTests
{
    public class AddressBookDb : LinqToDB.Data.DataConnection
    {
        public AddressBookDb() : base("AddressBook") { }

        public ITable<GroupData> Groups
        {
            get
            {
                return GetTable<GroupData>();
            }
        }
        public ITable<ContactData> Contacts
        {
            get
            {
                return GetTable<ContactData>();
            }
        }
    }
}