﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AddressBookWebTests
{
    public class UserAccountData
    {
        private string mUserName;
        private string mPassword;

        public UserAccountData(string pUserName, string pPassword)
        {
            this.mUserName = pUserName;
            this.mPassword = pPassword;
        }

        public string UserName
        {
            get
            {
                return mUserName;
            }
            set
            {
                mUserName = value;
            }
        }

        public string Password
        {
            get
            {
                return mPassword;
            }
            set
            {
                mPassword = value;
            }
        }
    }
}
