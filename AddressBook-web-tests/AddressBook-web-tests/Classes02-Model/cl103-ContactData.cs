﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using LinqToDB.Mapping;

namespace AddressBookWebTests
{
    [Table(Name = "addressbook")]
    public class ContactData : IEquatable<ContactData>, IComparable<ContactData>
    {
        private string mContAllPhones;
        private string mContAllEmails;
        private string mContInfoDetailsWithoutLineBreaksAndSpaces;
        public ContactData()
        {
        }
        public ContactData(string pName)
        {
            ContName = pName;
        }

        public ContactData(string pContactFirstName, string pContactLastName)
        {
            ContFirstName = pContactFirstName;
            ContLastName = pContactLastName;
        }

        // Overloaded, used in "Assert.AreEquals()"
        public bool Equals(ContactData other)
        {
            if (object.ReferenceEquals(other, null))
            {
                return false;   // ContactData is null
            }
            if (object.ReferenceEquals(this, other))
            {
                return true;    // Contact is the same
            }

            return (ContFirstName == other.ContFirstName)
                    && (ContLastName == other.ContLastName);
        }

        public int CompareTo(ContactData other)
        {
            if (object.ReferenceEquals(other, null))
            {
                return 1;   // Current object is greater..
            }

            //compare Last Names
            int lastNameDiff = ContLastName.CompareTo(other.ContLastName);
            if(lastNameDiff != 0)
            {   // Last names are different, return comparison result
                return lastNameDiff;
            }

            // Last Names are the same, compare The First Names
            return  ContFirstName.CompareTo(other.ContFirstName);  
        }

        // Overloaded, used in "Assert.AreEquals()"
        public override int GetHashCode()
        {
            int hash = 1;
            hash = hash * 13 + (ContFirstName == null ? 0 : ContFirstName.GetHashCode());
            hash = hash * 17 + (ContLastName == null ? 0 : ContLastName.GetHashCode());
            return hash;
        }

        public override string ToString()
        {
            return "Contact last name = " + ContLastName
                        + "\nContact first name = " + ContFirstName
                        + "\nContact name = " + ContName;
        }

        // ContName is not a column, it's internal field for ContactData data type
        public string ContName {get; set;}
        [Column(Name = "firstname")]
        public string ContFirstName {get; set;}
        [Column(Name = "lastname")]
        public string ContLastName {get; set;}
        [Column(Name = "company")]
        public string ContCompanyName {get; set;}

        [Column(Name = "address")]
        public string ContAddress { get; set; }
        [Column(Name = "home")]
        public string ContHomePhone { get; set; }
        [Column(Name = "mobile")]
        public string ContMobilePhone { get; set; }
        [Column(Name = "work")]
        public string ContWorkPhone { get; set; }

        public string ContAllEmails
        {
            get
            {
                if (mContAllEmails != null)
                {
                    return mContAllEmails;
                }
                else
                {
                    return (PhoneEmailCleanup(ContEmail) + PhoneEmailCleanup(ContEmail2) + PhoneEmailCleanup(ContEmail3)).Trim();
                }
            }
            set
            {
                mContAllEmails = value;
            }
        }
        public string ContAllPhones
        {
            get
            {
                if (mContAllPhones != null)
                {
                    return mContAllPhones;
                }
                else
                {
                    return (PhoneEmailCleanup(ContHomePhone) + PhoneEmailCleanup(ContMobilePhone) + PhoneEmailCleanup(ContWorkPhone)).Trim();
                }
            }
            set
            {
                mContAllPhones = value;
            }
        }

        public string ContInfoDetailsWithoutLineBreaksAndSpaces
        {
            get
            {
                if (mContInfoDetailsWithoutLineBreaksAndSpaces != null)
                {
                    return mContInfoDetailsWithoutLineBreaksAndSpaces;
                }
                else
                {
                    return
                        (ContFirstName + ContLastName + ContCompanyName
                        + ContAddress + ContAllPhones + ContAllEmails
                        + BirthdayCleanup(ContBirthDay) + BirthdayCleanup(ContBirthMonth) + ContBirthYear)
                        .Replace("\r\n", "").Replace(" ", "");
                }
            }
            set
            {
                mContInfoDetailsWithoutLineBreaksAndSpaces = value;
            }
        }

        private string PhoneEmailCleanup(string pString)   // Remove unneded symbols from phone/email, and add line break
        {
            if (pString == null || pString == "")
            {
                return "";
            }
            else
            {   // Regex [ -()] means any of the symbols ' ', '-', '(', ')' replace to ""
                return Regex.Replace(pString, "[ -()]", "") + "\r\n";
            }
        }
        private string BirthdayCleanup(string pDate)   // Removes "-" from dates
        {
            if (pDate == null || pDate == "")
            {
                return "";
            }
            else
            {
                return Regex.Replace(pDate, "[-]", "");
            }
        }

        [Column(Name = "email")]
        public string ContEmail { get; set; }
        [Column(Name = "email2")]
        public string ContEmail2 { get; set; }
        [Column(Name = "email3")]
        public string ContEmail3 { get; set; }

        [Column(Name = "bday")]
        public string ContBirthDay {get; set;}
        [Column(Name = "bmonth")]
        public string ContBirthMonth {get; set;}
        [Column(Name = "byear")]
        public string ContBirthYear { get; set; }
        [Column(Name = "id")]
        public string Id {get; set;}

        [Column(Name = "deprecated")]
        public string IsActive { get; set; }

        public static List<ContactData> GetAllContactsFromDB()
        {
            AddressBookDb lDb = new AddressBookDb();
            List<ContactData> lContactsFromDb = (
                from contacts in lDb.Contacts
                where contacts.IsActive == "0000-00-00 00:00:00"
                select contacts).ToList();
            lDb.Close();
            return lContactsFromDb;
        }

    }
}