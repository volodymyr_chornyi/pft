﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AddressBookWebTests
{
    class Square
    {
        private int privSize;

        public Square(int lSize)
        {
            this.privSize = lSize;
        }

        public int Size
        {
            get
            {
                return privSize;
            }
            set
            {
                privSize = value;
            }
        }
    }
}