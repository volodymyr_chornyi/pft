﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AddressBookWebTests
{
    class Circle
    {
        private int privRadius;

        public Circle (int lRadius)
        {
            this.privRadius = lRadius;
        }

        public int Radius
        {
            get
            {
                return privRadius;
            }
            set
            {
                privRadius = value;
            }
        }
    }
}