﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;


namespace AddressBookWebTests
{
    public class ApplicationManager
    {
        protected IWebDriver driver;
        protected string baseURL;
        protected bool acceptNextAlert;

        protected NavigationHelper helperNavigation;
        protected GroupHelper helperGroups;
        protected ContactHelper helperContacts;
        private static ThreadLocal<ApplicationManager> app =
            new ThreadLocal<ApplicationManager>();

        private ApplicationManager()
        {
            driver = new FirefoxDriver();
            driver.Manage().Window.Maximize();
            baseURL = "http://localhost:81/";
            acceptNextAlert = true;

            helperNavigation = new NavigationHelper(this);
            helperGroups = new GroupHelper(this);
            helperContacts = new ContactHelper(this);
        }

        ~ApplicationManager()
        {
            try
            {
                driver.Quit();
            }
            catch (Exception)
            {
                // Ignore errors if unable to close the browser
            }
        }

        public static ApplicationManager GetInstance()
        {
            // Multi-Threading Application Manager
            if (! app.IsValueCreated)
            { // create new Thread if not set
                var newInstance = new ApplicationManager();
                newInstance.hlpNavigator.GoToContactsPage();
                app.Value = newInstance;
            }
            return app.Value;
        }

        public string BaseURL
        {
            get
            {
                return baseURL;
            }
        }

        public IWebDriver Driver
        {
            get
            {
                return driver;
            }
        }

        public bool AcceptNextAlert
        {
            get
            {
                return acceptNextAlert;
            }
            set
            {
                acceptNextAlert = value;
            }
        }

        public NavigationHelper hlpNavigator
        {
            get
            {
                return helperNavigation;
            }
        }
        public GroupHelper hlpGroup
        {
            get
            {
                return helperGroups;
            }
        }
        public ContactHelper hlpContact
        {
            get
            {
                return helperContacts;
            }
        }
    }
}
