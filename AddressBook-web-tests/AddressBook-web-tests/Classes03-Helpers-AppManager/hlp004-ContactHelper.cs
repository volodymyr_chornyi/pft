﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Collections.Generic;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;

namespace AddressBookWebTests
{
    public class ContactHelper : HelperBase
    {

        public ContactHelper(ApplicationManager manager)
            : base(manager)
        {
        }
        public ContactHelper CreateContact(ContactData pContactData)
        {
            FillTheContactData(pContactData);
            ClickToSubmitContact();
            return this;
        }
        public ContactHelper EditContact(int pContactIndex, ContactData pContactData)
        {
            SelectContact(pContactIndex);
            ClickToEditContact(pContactIndex);
            FillTheContactData(pContactData);
            ConfirmUpdatingContact();
            return this;
        }

        private void FillTheContactData(ContactData pContactData)
        {
            TypeTextByName("firstname", pContactData.ContFirstName);
            TypeTextByName("lastname", pContactData.ContLastName);
            TypeTextByName("company", pContactData.ContCompanyName);

            SelectTextByElementName("bday", pContactData.ContBirthDay);
            SelectTextByElementName("bmonth", pContactData.ContBirthMonth);
            TypeTextByName("byear", pContactData.ContBirthYear);

            TypeTextByName("home", pContactData.ContHomePhone);
            TypeTextByName("mobile", pContactData.ContMobilePhone);
            TypeTextByName("work", pContactData.ContWorkPhone);

            TypeTextByName("email", pContactData.ContEmail);
            TypeTextByName("email2", pContactData.ContEmail2);
            TypeTextByName("email3", pContactData.ContEmail3);

            TypeTextByName("address", pContactData.ContAddress);
        }

        public ContactHelper RemoveContact()
        {
            ClickToDeleteContact();
            Assert.IsTrue(Regex.IsMatch(
                CloseAlertAndGetItsText(), "^Delete 1 addresses[\\s\\S]$"));
            return this;
        }

        private void ClickToDeleteContact()
        {
            ClickByXPath("//input[@value='Delete']");
            mContactCashe = null;
        }

        private void ClickToSubmitContact()
        {
            ClickByName("submit");
            mContactCashe = null;
        }

        private void ConfirmUpdatingContact()
        {
            ClickByName("update");
            mContactCashe = null;
        }

        public ContactHelper SelectContact(int pContactIndex)
        {   // Index starts with "0"
            ClickByXPath("(//input[@name='selected[]'])[" + (pContactIndex + 1) + "]");
            return this;
        }
        public ContactHelper SelectContact(string pContactId)
        {
            ClickByID(pContactId);
            return this;
        }
        private ContactHelper ClickToSeeContactDetails(int pContactIndex)
        {   // Index starts with "0"
            ClickByXPath("(//img[@alt='Details'])[" + (pContactIndex + 1) + "]");
            return this;
        }
        private ContactHelper ClickToEditContact(int pContactIndex)
        {   // Index starts with "0"
            ClickByXPath("(//img[@alt='Edit'])[" + (pContactIndex + 1) + "]");
            return this;
        }

        public bool AtLeastOneContactPresent()
        {
            return IsElementPresent(By.XPath("(//img[@alt='Edit'])[1]"));
        }

        private List<ContactData> mContactCashe = null;

        public List<ContactData> GetAllContactsFromUI()
        {
            if (mContactCashe == null)
            {
                mContactCashe = new List<ContactData>();
                manager.hlpNavigator.GoToContactsPage();

                int lDataRowCount = driver.FindElements(By.XPath("//tr[@name='entry']")).Count;
                IWebElement lTBody = driver.FindElement(By.XPath("//*[@id='maintable']/tbody"));

                string lFirstName; string lLastName;
                for (int i = 1; i <= lDataRowCount; i++)
                {
                    lFirstName = lTBody.FindElement(By.XPath("//tr[" + (i + 1) + "]/td[3]")).Text;
                    lLastName = lTBody.FindElement(By.XPath("//tr[" + (i + 1) + "]/td[2]")).Text;

                    mContactCashe.Add(new ContactData(lFirstName, lLastName) {
                        Id = lTBody
                            .FindElement((By.XPath("//tr[" + (i + 1) + "]/td[1]")))
                            .FindElement(By.TagName("input"))
                            .GetAttribute("value") // ID in browser
                        });
                }
            }
            return new List<ContactData>(mContactCashe);
        }
        
        public int GetContactCount()
        {
            manager.hlpNavigator.GoToContactsPage();
            return driver.FindElements(By.XPath("//tr[@name='entry']")).Count;
        }

        public ContactData GetContactInformationFromTable(int pContactIndex)
        {
            manager.hlpNavigator.GoToContactsPage();
            IList<IWebElement> cells = driver.FindElements(By.Name("entry"))[pContactIndex]
                .FindElements(By.TagName("td"));
            string lLastName = cells[1].GetAttribute("innerHTML"); // .Text replaces two or more spaces into one space, therefore we use 'innerHTML'
            string lFirstName = cells[2].GetAttribute("innerHTML"); // .Text replaces two or more spaces into one space, therefore we use 'innerHTML'
            string lAddress = cells[3].Text.Replace(" ", "");   // We replace spaces, since they are auto-replaced on Details-page.
            string lAllEmails = cells[4].Text;
            string lAllPhones = cells[5].Text;

            return new ContactData(lFirstName, lLastName)
            {
                ContAddress = lAddress,
                ContAllEmails = lAllEmails,
                ContAllPhones = lAllPhones
            };
        }

        public int CountContactsContainingSearchString(string pSearchString)
        {
            manager.hlpNavigator.GoToContactsPage();
            IList<IWebElement> lCells = driver.FindElements(By.Name("entry"));
            int iCount = 0;

            foreach (IWebElement lCell in lCells)
            {
                if (lCell.Text.Contains(pSearchString))
                {
                    iCount++;
                }
            }
            return iCount;
        }

        // Use the 2nd parameter "pSetupLastNameAsFirstNamePlusLastName" to use either FirstName and LastName
        // in separate fields (false)
        // or concatenate these into LastName and leave Firstname empty(true).
        public ContactData GetContactInformationFromEditForm(int pContactIndex)
        {
            manager.hlpNavigator.GoToContactsPage();
            ClickToEditContact(pContactIndex);

            string lFirstName = GetValueByName("firstname").Trim();
            string lLastName = GetValueByName("lastname").Trim();
            string lAddress = GetValueByName("address").Replace(" ", "");

            string lCompany = GetValueByName("company");

            string lHomePhone = GetValueByName("home");
            string lMobilePhone = GetValueByName("mobile");
            string lWorkPhone = GetValueByName("work");

            string lEmail = GetValueByName("email");
            string lEmail2 = GetValueByName("email2");
            string lEmail3 = GetValueByName("email3");

            string lBirthDay = GetSelectedOptionByName("bday");
            string lBirthMonth = GetSelectedOptionByName("bmonth");
            string lBirthYear = GetValueByName("byear");

            return new ContactData(lFirstName, lLastName)
            {
                ContAddress = lAddress,
                ContCompanyName = lCompany,
                ContHomePhone = lHomePhone,
                ContMobilePhone = lMobilePhone,
                ContWorkPhone = lWorkPhone,
                ContEmail = lEmail,
                ContEmail2 = lEmail2,
                ContEmail3 = lEmail3,
                ContBirthDay = lBirthDay,
                ContBirthMonth = lBirthMonth,
                ContBirthYear = lBirthYear
            };
        }

        public void SetTheSearchFilter(string pSearchString)
        {
            TypeTextByName("searchstring", pSearchString);  // Search -- type into the search list
        }
        public void ResetTheSearchFilter()
        {
            driver.Navigate().GoToUrl(baseURL + "addressbook/"); // Clear the search list
        }

        public ContactData GetContactInformationFromInfoScreen(int pContactIndex)
        {
            manager.hlpNavigator.GoToContactsPage();
            ClickToSeeContactDetails(pContactIndex);

            ContactData lContact = new ContactData (null);

            string[] lContInfoRows = driver.FindElement(By.Id("content")).Text.Split('\n');
            string lContInfoDetails = "";

            for (int i = 0; i < lContInfoRows.Length; i++)
            {
                lContInfoRows[i] = lContInfoRows[i].Replace("\r", "");

                if (lContInfoRows[i].Contains(":"))
                {   // Phones
                    if (Regex.Matches(lContInfoRows[i], "H: ").Count == 1)
                    {   // Only one "H: ", so this is home phone
                        lContInfoRows[i] = CleanupPhoneOnDetailsPage(lContInfoRows[i], "H:");
                    }
                    else if (Regex.Matches(lContInfoRows[i], "M: ").Count == 1)
                    {   // Only one "M: ", so this is mobile phone
                        lContInfoRows[i] = CleanupPhoneOnDetailsPage(lContInfoRows[i], "M:");
                    }
                    else if (Regex.Matches(lContInfoRows[i], "W: ").Count == 1)
                    {   // Only one "W: ", so this is work phone
                        lContInfoRows[i] = CleanupPhoneOnDetailsPage(lContInfoRows[i], "W:");
                    }
                }
                if (lContInfoRows[i].Contains("@"))
                {   // Emails, we search for domain pattern
                    var lDomainPattern = lContInfoRows[i].Split(new char[] { '@', ' ' })[1];
                    var lMatchesCount = Regex.Matches(lContInfoRows[i], lDomainPattern).Count;
                    if (lMatchesCount > 1)
                    {
                        string[] lEmailParts = lContInfoRows[i].Split(new char[] { '@', ' ' });
                        // We split the following: <<email@test.com (www.test.com)>> into parts:
                        // email    // test.com // (www.test.com)
                        // And then remove the 3rd part "(www.test.com)" -- lDomainPattern[2]
                        if (lEmailParts.Length == 3)
                        {   // Remove domain part
                            lContInfoRows[i] = lContInfoRows[i].Replace(lEmailParts[2], "");
                        }
                    }
                }

                if (lContInfoRows[i].Contains("Birthday"))
                {   // Birthday
                    string[] lBirthdayParts = lContInfoRows[i].Split(' ');
                    // We split the following: <<Birthday 15. March 1986 (28)>> into parts
                    if (lBirthdayParts.Length >= 4)
                    {   // Only for the case it's Birthday field, cleanup birthday
                        lContInfoRows[i] = lBirthdayParts[1].Replace(".", "")
                            + lBirthdayParts[2] + lBirthdayParts[3];
                    }
                }
                lContInfoDetails += lContInfoRows[i];   // Add cleaned data to lContInfoDetails
            }
            lContInfoDetails = lContInfoDetails.Replace(" ", "");
            lContact.ContInfoDetailsWithoutLineBreaksAndSpaces = lContInfoDetails;
            return lContact;
        }
        
        private static string CleanupPhoneOnDetailsPage(string pContInfoRows, string pPhoneType)
        {
            return pContInfoRows.Replace(pPhoneType, "")
                .Replace(" ", "").Replace("(", "").Replace(")", "");
        }

        public int GetNumberOfSearchResults()
        {
            manager.hlpNavigator.GoToContactsPage();
            string lText = driver.FindElement(By.TagName("label")).Text;
            Match m = new Regex(@"\d+").Match(lText);
            return Int32.Parse(m.Value);
        }

        public int GetNumberOfDisplayedRows()
        {
            manager.hlpNavigator.GoToContactsPage();
            IWebElement lTBody = driver.FindElement(By.CssSelector("#maintable>tbody"));
            return lTBody.FindElements(By.XPath("//tr[@name='entry']")).Count -
                lTBody.FindElements(By.XPath("//tr[@name='entry'][@style='display: none;']")).Count;
        }
    }
}