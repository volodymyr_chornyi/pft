﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;

namespace AddressBookWebTests
{
    public class HelperBase
    {
        protected IWebDriver driver;
        protected string baseURL;
        protected ApplicationManager manager;
        protected bool acceptNextAlert;

        public HelperBase(ApplicationManager manager)
        {
            this.manager = manager;
            this.driver = manager.Driver;
            this.baseURL = manager.BaseURL;
            this.acceptNextAlert = manager.AcceptNextAlert;
        }

        public bool IsAlertPresent()
        {
            try
            {
                driver.SwitchTo().Alert();
                return true;
            }
            catch (NoAlertPresentException)
            {
                return false;
            }
        }

        public bool IsElementPresent(By by)
        {
            try
            {
                driver.FindElement(by);
                return true;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
        }

        public string CloseAlertAndGetItsText()
        {
            try
            {
                IAlert alert = driver.SwitchTo().Alert();
                string alertText = alert.Text;
                if (acceptNextAlert)
                {
                    alert.Accept();
                }
                else
                {
                    alert.Dismiss();
                }
                return alertText;
            }
            finally
            {
                acceptNextAlert = true;
            }
        }

        public void TypeTextByLocator(By pLocator, string pText)
        {
            if (pText != null)
            {
                driver.FindElement(pLocator).Clear();
                driver.FindElement(pLocator).SendKeys(pText);
            }
        }

        public void TypeTextByName(string pName, string pText)
        {
            if (pText != null)
            {
                driver.FindElement(By.Name(pName)).Clear();
                driver.FindElement(By.Name(pName)).SendKeys(pText);
            }
        }

        public void SelectTextByElementName(string pName, string pText)
        {
            if (pText != null)
            {
                new SelectElement(driver.FindElement(By.Name(pName))).SelectByText(pText);
            }
        }

        public void ClickByID(string pId)
        {
            driver.FindElement(By.Id(pId)).Click();
        }

        public void ClickByName(string pName)
        {
            driver.FindElement(By.Name(pName)).Click();
        }

        public void ClickByXPath(string pXpath)
        {
            driver.FindElement(By.XPath(pXpath)).Click();
        }

        public void ClickByLinkText(string pLinkText)
        {
            driver.FindElement(By.LinkText(pLinkText)).Click();
        }
        public void ClickByCssSelector(string pCssSelector)
        {
            driver.FindElement(By.CssSelector(pCssSelector)).Click();
        }
        public string GetValueByName(string pName)
        {
            return driver.FindElement(By.Name(pName)).GetAttribute("value");
        }
        public string GetSelectedOptionByName(string pName)
        {
            return driver.FindElement(By.Name(pName)).FindElements(By.XPath("./option[@selected]"))[0].Text;
        }
    }
}
