﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;

namespace AddressBookWebTests
{
    public class NavigationHelper : HelperBase
    {
        public NavigationHelper(ApplicationManager manager)
            : base(manager)
        {
        }

        public NavigationHelper LoginIntoAddressBook(UserAccountData lAccount)
        {
            if (isCurrentUserLoggedIn(lAccount))
            {
                return this; // No need to login again
            }
            LogoutIfLoggedIn();

            TypeTextByName("user", lAccount.UserName);
            TypeTextByName("pass", lAccount.Password);

            ClickByCssSelector("input[type=\"submit\"]");
            return this;
        }

        public NavigationHelper LogoutIfLoggedIn()
        {
            if (isLoggedIn())
            {
                driver.FindElement(By.LinkText("Logout")).Click();
            }
            return this;
        }

        public NavigationHelper GoToGroupsPage()
        {
            if (! isOnGoupsPage())
            {
                ClickByLinkText("groups");
            }
            return this;
        }

        public NavigationHelper GoToAddNewContactPage()
        {
            if (! isOnAddNewContactPage())
            {
                ClickByLinkText("add new");
            }
            return this;
        }

        public NavigationHelper GoToContactsPage()
        {
            if (! isOnContactsPage())
            {
                driver.Navigate().GoToUrl(baseURL + "addressbook/");
            }
            return this;
        }

        public bool isLoggedIn()
        {
            return IsElementPresent(By.LinkText("Logout"));
        }

        public bool isCurrentUserLoggedIn(UserAccountData lAccount)
        {
            return isLoggedIn()
                && GetLoggedUserName() == lAccount.UserName;
        }

        public string GetLoggedUserName()
        {
            string lText = driver.FindElement(By.XPath(".//*[@id='top']/form/b")).Text;
            return lText.Substring(1, lText.Length - 2);
        }

        public bool isOnGoupsPage()
        {
            return driver.Url.Contains("group.php")
                && IsElementPresent(By.Name("new"));
        }
        private bool isOnAddNewContactPage()
        {
            return driver.Url.Contains("edit.php")
                && IsElementPresent(By.XPath("//input[@value='Enter']"));
        }
        private bool isOnContactsPage()
        {
            return driver.Url == baseURL + "addressbook/"
                && IsElementPresent(By.XPath("//input[@value='Send e-Mail']"));
        }
    }
}