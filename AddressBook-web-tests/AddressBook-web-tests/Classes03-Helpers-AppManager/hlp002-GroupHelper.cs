﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;

namespace AddressBookWebTests
{
    public class GroupHelper : HelperBase
    {
        public GroupHelper(ApplicationManager manager)
            : base(manager)
        {
        }

        public GroupHelper CreateGroup(GroupData lGroupData)
        {
            FillTheGroupDataAndClickToSubmit(lGroupData);
            return this;
        }

        public GroupHelper EditGroup(GroupData lGroup)
        {
            InitExistingGroupModification();
            FillTheGroupDataAndClickToSubmit(lGroup);
            return this;
        }

        public GroupHelper InitNewGroupCreation()
        {
            ClickByName("new");
            return this;
        }
        public GroupHelper InitExistingGroupModification()
        {
            ClickByName("edit");
            return this;
        }

        public GroupHelper FillTheGroupDataAndClickToSubmit(GroupData lGroupData)
        {
            TypeTextByName("group_name", lGroupData.GroupName);
            TypeTextByName("group_header", lGroupData.GroupHeader);
            TypeTextByName("group_footer", lGroupData.GroupFooter);

            // Find and click the "Enter" button for New Group or the "Update" button for existging group
            ClickByXPath("//input[@type='submit']");
            groupCashe = null;
            return this;
        }

        public GroupHelper SelectGroup(int pGroupIndex)
        {
            ClickByXPath("(//input[@name='selected[]'])[" + (pGroupIndex+1) + "]");
            return this;
        }
        public GroupHelper SelectGroup(string pGroupId)
        {
            ClickByXPath("//input[@value='" + pGroupId + "']");
            return this;
        }

        public GroupHelper RemoveGroup()
        {
            ClickByXPath("//input[@name='delete']");
            groupCashe = null;
            return this;
        }

        public bool AtLeastOneGroupPresent()
        {
            return IsElementPresent(By.XPath("(//input[@name='selected[]'])[1]"));
        }

        private List<GroupData> groupCashe = null;

        public List<GroupData> GetAllGroupsFromUI()
        {
            if (groupCashe == null)
            {
                groupCashe = new List<GroupData>();
                manager.hlpNavigator.GoToGroupsPage();
                ICollection<IWebElement> elements = driver.FindElements(By.CssSelector("span.group"));
                foreach (IWebElement element in elements)
                {
                    groupCashe.Add(
                        new GroupData(null){   // Create new group [constructor]
                            Id = element.FindElement(By.TagName("input")).GetAttribute("value") // ID in browser
                        }
                    );
                }
                string lAllGroupNames = driver.FindElement(By.CssSelector("div#content form")).Text;
                string[] lParts = lAllGroupNames.Split('\n');
                int lShift = groupCashe.Count - lParts.Length;  // Empty group names are not going to "parts". So we count the 'shift' between group cashe and parts
                for (int i = 0; i < groupCashe.Count;  i++)
                {
                    if (i < lShift)
                    {   // For every group with empty name
                        groupCashe[i].GroupName = "";
                    }
                    else
                    {
                        groupCashe[i].GroupName = lParts[i-lShift].Trim();
                    }
                }
            }
            return new List<GroupData>(groupCashe);
        }

        public int GetGroupCount()
        {
            manager.hlpNavigator.GoToGroupsPage();
            return driver.FindElements(By.CssSelector("span.group")).Count;
        }
    }
}
