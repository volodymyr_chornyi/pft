﻿using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;

namespace AddressBookWebTests
{
    [TestClass]
    public class UnitTestMSTEST
    {
        [TestMethod]
        public void Test_001_Square()
        {
            Square s1 = new Square(3);
            Square s2 = new Square(5);
            Square s3 = s1;

            Assert.AreEqual(s1.Size, 3);
            Assert.AreEqual(s2.Size, 5);
            Assert.AreEqual(s3.Size, 3);

            s3.Size = 6;
            Assert.AreEqual(s1.Size, 6);
        }

        //[TestMethod]
        public void Test_008_TestTimeMeasure()
        {
            DateTime lStart = DateTime.Now;
            DateTime lEnd = DateTime.Now;
            System.Console.Out.WriteLine(lEnd.Subtract(lStart));
        }

        // [TestMethod]
        public void Test_11111_ContactDataFromCsvFile()
        {
            List<ContactData> lContacts = new List<ContactData>();
            string[] lLines = File.ReadAllLines(@"UnitTests03-TestData\contacts.csv");
            foreach (string lLine in lLines)
            {
                string[] lParts = lLine.Split(',');
                lContacts.Add(new ContactData(lParts[0])
                {
                    ContFirstName = lParts[1],
                    ContLastName = lParts[2],
                    ContCompanyName = lParts[3],
                    ContBirthDay = lParts[4],
                    ContBirthMonth = lParts[5],
                    ContBirthYear = lParts[6],
                    ContHomePhone = lParts[7],
                    ContMobilePhone = lParts[8],
                    ContEmail = lParts[9],
                    ContEmail3 = lParts[10],
                    ContAddress = lParts[11].Replace("xxxxxxxxxxxxxxx", "\r\n") // Back replace
                });
            }
            foreach (ContactData lC in lContacts)
            {
                System.Console.Out.Write(lC.ContAddress);
            }
        }

        [TestMethod]
        public void Test_002_Circle()
        {
            Circle s1 = new Circle(3);
            Circle s2 = new Circle(5);
            Circle s3 = s1;

            Assert.AreEqual(s1.Radius, 3);
            Assert.AreEqual(s2.Radius, 5);
            Assert.AreEqual(s3.Radius, 3);

            s3.Radius = 6;
            Assert.AreEqual(s1.Radius, 6);
        }

        [TestMethod]
        public void Test_003_IfThen()
        {
            double total = 1200;
            bool vipClient = true;

            if (total >= 1000 && vipClient)
            {
                total = total * 0.9;
                System.Console.Out.Write("Discount 10%. Total = " + total);
            }
            else
            {
                System.Console.Out.Write("No Discount. Total = " + total);
            }
        }

        [TestMethod]
        public void Test_004_Loops()
        {
            string[] s = new string[] { "I", "want", "to", "go" };
            ///// OLD CONSTRUCTION
            for (int i = 0; i < 10; i++)
            {
                    System.Console.Out.Write("i = " + i + "");
            }
            System.Console.Out.Write("\n");
            ///// OLD CONSTRUCTION
            for (int i = 0; i < s.Length; i++)
            {
                System.Console.Out.Write(s[i] + " ");
            }
            System.Console.Out.Write("\n");
            ///// NEW CONSTRUCTION
            foreach (string element in s)
            {
                System.Console.Out.Write(element + " ");
            }
            System.Console.Out.Write("\n");
        }

        //[TestMethod]
        public void Test_005_While()
        {
            IWebDriver lDriver;
            lDriver = new FirefoxDriver();
            int iCount = 0;

            // wait for element, or stop after 30 iterations (30 seconds)
            while (lDriver.FindElements(By.Id("test")).Count == 0 && iCount < 30)
            {
                System.Threading.Thread.Sleep(1000);
                iCount++;
            }
        }

        //[TestMethod]
        public void Test_006_DoWhile()
        {
            IWebDriver lDriver;
            lDriver = new FirefoxDriver();
            int iCount = 0;

            // At least loop operations will be executed ones (unlike WHILE, when this loop may not be executed)
            do
            {
                System.Threading.Thread.Sleep(1000);
                iCount++;
            }
            while (lDriver.FindElements(By.Id("test")).Count == 0 && iCount < 30);
        }

        /*
         * maybe will use this in future
        public List<ContactData> GetContactsList_ForFutureReUse()
        {
            List<ContactData> lContacts = new List<ContactData>();
            string lFirstName = ""; string lLastName = "";
            manager.hlpNavigator.GoToContactsPage();

            ICollection<IWebElement> rowCollection = driver.FindElements(By.XPath("//*[@id='maintable']/tbody/tr"));
            System.Console.Out.Write("Number of data rows in this table: " + (rowCollection.Count - 1));

            int iRowNum = 1;
            foreach (IWebElement rowElement in rowCollection)
            {
                ICollection<IWebElement> colCollection = rowElement.FindElements(By.XPath("td"));
                int iColNum = 1;
                foreach (IWebElement colElement in colCollection)
                {
                    if (iColNum == 2) // Last name
                    {
                        lLastName = colElement.Text;
                    }
                    else if (iColNum == 3)
                    {
                        lFirstName = colElement.Text;
                        System.Console.Out.Write("Row " + iRowNum + " Column " + iColNum + " Data " +
                            (colElement.Text == "" ? "EMPTY" : colElement.Text));
                        break;  // There is no reason to loop further
                    }

                    System.Console.Out.Write("Row " + iRowNum + " Column " + iColNum + " Data " +
                        (colElement.Text == "" ? "EMPTY" : colElement.Text));
                    iColNum++;
                }
                if (iRowNum != 1)
                {   // First row is TH (table header row), so not to add contact
                    lContacts.Add(new ContactData(lFirstName, lLastName));
                }
                iRowNum++;
            }

            return lContacts;
        }
         * */

    }
}