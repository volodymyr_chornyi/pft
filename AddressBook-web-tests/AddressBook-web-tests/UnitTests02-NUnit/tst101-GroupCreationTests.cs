﻿using System;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using Excel = Microsoft.Office.Interop.Excel;
using Newtonsoft.Json;
using NUnit.Framework;

namespace AddressBookWebTests
{
    [TestFixture]
    public class GroupCreationTestsNUNIT : GroupTestBase
    {
        public static IEnumerable<GroupData> RandomGroupDataProvider()
        {
            List<GroupData> lGroups = new List<GroupData>();
            for (int i = 0; i < 3; i++)
            {
                lGroups.Add(new GroupData(GenerateRandomString(30))
                {
                    GroupHeader = GenerateRandomString(100),
                    GroupFooter = GenerateRandomString(100)
                });
            }
                return lGroups;
        }

        public static IEnumerable<GroupData> GroupDataFromCsvFile()
        {
            List<GroupData> lGroups = new List<GroupData>();
            string[] lLines = File.ReadAllLines(@"UnitTests03-TestData\groups.csv");
            foreach (string lLine in lLines)
            {
                string[] lParts = lLine.Split(',');
                lGroups.Add(new GroupData(lParts[0])
                {
                    GroupHeader = lParts[1],
                    GroupFooter = lParts[2]
                });
            }

            return lGroups;
        }

        public static IEnumerable<GroupData> GroupDataFromXmlFile()
        {
            List<GroupData> lGroups = new List<GroupData>();

            return (List<GroupData>) new
                XmlSerializer(typeof(List<GroupData>))
                .Deserialize(new StreamReader(@"UnitTests03-TestData\groups.xml"));
        }

        public static IEnumerable<GroupData> GroupDataFromJsonFile()
        {
            return JsonConvert.DeserializeObject<List<GroupData>>(
                File.ReadAllText(@"UnitTests03-TestData\groups.json")
                );
        }

        public static IEnumerable<GroupData> GroupDataFromExcelFile()
        {
            List<GroupData> lGroups = new List<GroupData>();

            Excel.Application lExcelApp = new Excel.Application();
            string lFullPath = Path.Combine(Directory.GetCurrentDirectory(), @"UnitTests03-TestData\groups.xlsx");
            Excel.Workbook lWorkBook = lExcelApp.Workbooks.Open(lFullPath);
            Excel.Worksheet lSheet = lWorkBook.ActiveSheet;
            Excel.Range lRange = lSheet.UsedRange;

            for (int i = 1; i <= lRange.Rows.Count; i++)
            {
                lGroups.Add(new GroupData()
                {
                    GroupName = lRange.Cells[i, 1].Value,
                    GroupHeader = lRange.Cells[i, 2].Value,
                    GroupFooter = lRange.Cells[i, 3].Value
                });
            }

            lWorkBook.Close();
            lExcelApp.Quit();

            return lGroups;
        }

        [Test]
        public void Test_101_CreateGroupWithExactSetOfData()
        {
            GroupData lGroup = new GroupData("q");
            lGroup.GroupFooter = "yyy";
            lGroup.GroupHeader = "xxx";

            List<GroupData> oldGroups = GroupData.GetAllGroupsFromDB();
            app.hlpGroup
                .InitNewGroupCreation()
                .CreateGroup(lGroup);

            // If tests often fail -- we verify groups list
            Assert.AreEqual(oldGroups.Count + 1, app.hlpGroup.GetGroupCount());

            List<GroupData> newGroups = GroupData.GetAllGroupsFromDB();
            oldGroups.Add(lGroup);
            oldGroups.Sort();
            newGroups.Sort();
            Assert.AreEqual(oldGroups, newGroups);
        }

        [Test, TestCaseSource("GroupDataFromJsonFile")]
        public void Test_102_CreateGroupUsingDataProvider(GroupData pGroup)
        {
            List<GroupData> oldGroups = GroupData.GetAllGroupsFromDB();

            app.hlpGroup
                .InitNewGroupCreation()
                .CreateGroup(pGroup);

            // If tests often fail -- we verify groups list
            Assert.AreEqual(oldGroups.Count + 1, app.hlpGroup.GetGroupCount());

            List<GroupData> newGroups = GroupData.GetAllGroupsFromDB();
            oldGroups.Add(pGroup);
            oldGroups.Sort();
            newGroups.Sort();
            Assert.AreEqual(oldGroups, newGroups);
        }

        [Test]
        public void Test_103_CreateGroupWithBadName()
        {
            GroupData lGroup = new GroupData("a'a");
            lGroup.GroupFooter = "";
            lGroup.GroupHeader = "";

            List<GroupData> oldGroups = GroupData.GetAllGroupsFromDB();
            app.hlpGroup
                .InitNewGroupCreation()
                .CreateGroup(lGroup);

            Assert.AreEqual(oldGroups.Count + 1, app.hlpGroup.GetGroupCount());

            List<GroupData> newGroups = GroupData.GetAllGroupsFromDB();
            oldGroups.Add(lGroup);
            oldGroups.Sort();
            newGroups.Sort();
            Assert.AreEqual(oldGroups, newGroups);
        }
    }
}
