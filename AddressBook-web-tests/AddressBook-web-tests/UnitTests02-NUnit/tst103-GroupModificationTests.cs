﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Collections.Generic;
using NUnit.Framework;

namespace AddressBookWebTests
{
    [TestFixture]
    public class GroupModificationTestsNUNIT : GroupTestBase
    {
        [Test]
        public void Test_106_EditGroupData()
        {
            GroupData lGroup = new GroupData(GenerateRandomEnglishString(false, true, 40));
            lGroup.GroupFooter = GenerateRandomEnglishString(false, true, 40);
            lGroup.GroupHeader = GenerateRandomEnglishString(false, true, 40);

            EnsureAtLeastOneGroupPresent();
            
            List<GroupData> oldGroups = GroupData.GetAllGroupsFromDB();

            int lIndexToEdit = GenerateRandomInteger(0, oldGroups.Count - 1); // Random index to edit
            string idOfGroupToEdit = oldGroups[lIndexToEdit].Id;

            oldGroups[lIndexToEdit].GroupName = lGroup.GroupName;
            oldGroups[lIndexToEdit].GroupHeader = lGroup.GroupHeader;
            oldGroups[lIndexToEdit].GroupFooter = lGroup.GroupFooter;

            app.hlpGroup.SelectGroup(idOfGroupToEdit) // Select a group in browser with exact Id
                .EditGroup(lGroup); // Edit selected group in browser

            List<GroupData> newGroups = GroupData.GetAllGroupsFromDB();
            oldGroups.Sort();
            newGroups.Sort();
            Assert.AreEqual(oldGroups, newGroups);
        }
    }
}
