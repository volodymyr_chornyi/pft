﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using NUnit.Framework;

namespace AddressBookWebTests
{
    [TestFixture]
    public class LoginTestsNUNIT : TestBase
    {
        [Test]
        public void Test_021_LoginWithValidCredentials()
        {
            // Prepare user account data
            var lAccount = new UserAccountData("admin", "secret");

            // Verification
            app.hlpNavigator
                .LogoutIfLoggedIn()
                .LoginIntoAddressBook(lAccount);

            // Validation
            Assert.IsTrue(app.hlpNavigator.isCurrentUserLoggedIn(lAccount));
        }
        [Test]
        public void Test_022_LoginWithInvalidCredentials()
        {
            // Prepare user account data
            var lAccount = new UserAccountData("admin", "test");

            // Verification
            app.hlpNavigator
                .LogoutIfLoggedIn()
                .LoginIntoAddressBook(lAccount);

            // Validation
            Assert.IsFalse(app.hlpNavigator.isCurrentUserLoggedIn(lAccount));
        }
    }
}