﻿using System;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using Newtonsoft.Json;
using NUnit.Framework;


namespace AddressBookWebTests
{
    [TestFixture]
    public class ContactCreationTestsNUNIT : ContactTestBase
    {
        public static IEnumerable<ContactData> RandomContactDataProvider()
        {
            List<ContactData> lContacts = new List<ContactData>();
            var value = RandomEnumValue<eMonth>().ToString();

            for (int i = 0; i < 3; i++)
            {
                lContacts.Add(new ContactData(GenerateRandomEnglishString(true, true, 20))
                {
                    ContFirstName = GenerateRandomEnglishString(true, false, 40),
                    ContLastName = GenerateRandomEnglishString(true, false, 40),
                    ContCompanyName = GenerateRandomEnglishString(true, false, 40),
                    ContBirthDay = GenerateRandomInteger(1, 31).ToString(),
                    ContBirthMonth = RandomEnumValue<eMonth>().ToString(), // "February",
                    ContBirthYear = GenerateRandomInteger(1900, DateTime.Now.Year - 1).ToString(),  // last possible year is previous

                    ContHomePhone = "+380" + GenerateRandomPhoneNumber(9),
                    ContMobilePhone = "+380" + GenerateRandomPhoneNumber(9),

                    ContEmail = GenerateRandomEmail(),
                    ContEmail3 = GenerateRandomEmail(),

                    ContAddress = GenerateRandomEnglishString(true, false, 20) + "\r\n"
                                    + GenerateRandomEnglishString(true, false, 20) + "\r\n"
                                    + GenerateRandomEnglishString(true, false, 20)
                });
            }
            return lContacts;
        }

        public static IEnumerable<ContactData> ContactDataFromCsvFile()
        {
            List<ContactData> lContacts = new List<ContactData>();
            string[] lLines = File.ReadAllLines(@"UnitTests03-TestData\contacts.csv");
            foreach (string lLine in lLines)
            {
                string[] lParts = lLine.Split(',');
                lContacts.Add(new ContactData(lParts[0])
                {
                    ContFirstName = lParts[1],
                    ContLastName = lParts[2],
                    ContCompanyName = lParts[3],
                    ContBirthDay = lParts[4],
                    ContBirthMonth = lParts[5],
                    ContBirthYear = lParts[6],
                    ContHomePhone = lParts[7],
                    ContMobilePhone = lParts[8],
                    ContEmail = lParts[9],
                    ContEmail3 = lParts[10],
                    ContAddress = lParts[11].Replace("xxxxxxxxxxxxxxx", "\r\n") // Back replace that was replaced by saving the file
                });
            }
            return lContacts;
        }

        public static IEnumerable<ContactData> ContactDataFromXmlFile()
        {
            List<ContactData> lGroups = new List<ContactData>();

            return (List<ContactData>)new
                XmlSerializer(typeof(List<ContactData>))
                .Deserialize(new StreamReader(@"UnitTests03-TestData\contacts.xml"));
        }

        public static IEnumerable<ContactData> ContactDataFromJsonFile()
        {
            return JsonConvert.DeserializeObject<List<ContactData>>(
                File.ReadAllText(@"UnitTests03-TestData\contacts.json")
                );
        }

        [Test]
        public void Test_111_CreateContactSampleData ()
        {
            ContactData lContact = new ContactData("test");
            lContact.ContFirstName = "FirstName3";
            lContact.ContLastName = "LastName3";
            lContact.ContCompanyName = "TestCompany";
            lContact.ContBirthDay = "7";
            lContact.ContBirthMonth = "February";
            lContact.ContBirthYear = "1990";

            lContact.ContHomePhone = "+38065 645 12 45";
            lContact.ContMobilePhone = "+38096 322 32 45";

            lContact.ContEmail = "test1@home.net";
            lContact.ContEmail3 = "test23@ado.ck.com";

            lContact.ContAddress = @" dvdsfvsdfvsdfv
                                        sdevjnsdfvkljnsdfvnksdf
                                        wwwwwwwwwwwwwwwww";

            List<ContactData> oldContacts = ContactData.GetAllContactsFromDB();
            app.hlpNavigator.GoToAddNewContactPage();
            app.hlpContact.CreateContact(lContact);

            // If tests often fail -- we verify contacts list (so, fail the test now)
            Assert.AreEqual(oldContacts.Count + 1, app.hlpContact.GetContactCount());

            List<ContactData> newContacts = ContactData.GetAllContactsFromDB();
            oldContacts.Add(lContact);
            oldContacts.Sort();
            newContacts.Sort();
            Assert.AreEqual(oldContacts, newContacts);
        }

        [Test, TestCaseSource("ContactDataFromCsvFile")]
        public void Test_112_CreateContactRandomDataProvider(ContactData pContact)
        {
            List<ContactData> oldContacts = ContactData.GetAllContactsFromDB();
            app.hlpNavigator.GoToAddNewContactPage();
            app.hlpContact.CreateContact(pContact);

            // If tests often fail -- we verify contacts list (so, fail the test now)
            Assert.AreEqual(oldContacts.Count + 1, app.hlpContact.GetContactCount());

            List<ContactData> newContacts = ContactData.GetAllContactsFromDB();
            oldContacts.Add(pContact);
            oldContacts.Sort();
            newContacts.Sort();
            Assert.AreEqual(oldContacts, newContacts);
        }
    }
}
