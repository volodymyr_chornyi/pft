﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Collections.Generic;
using NUnit.Framework;

namespace AddressBookWebTests
{
    [TestFixture]
    public class ContactRemovalTestsNUNIT : ContactTestBase
    {
        [Test]
        public void Test_115_RemoveContact ()
        {
            EnsureAtLeastOneContactPresent();
            List<ContactData> oldContacts = ContactData.GetAllContactsFromDB();
            int lIndexToRemove = GenerateRandomInteger(0, oldContacts.Count - 1);
            string idOfContactToRemove = oldContacts[lIndexToRemove].Id;

            app.hlpContact.SelectContact(idOfContactToRemove) // Select a contact in browser with exact Id
                .RemoveContact(); // Remove selected contact in browser

            // If tests often fail -- we verify contacts list (so, fail the test now)
            Assert.AreEqual(oldContacts.Count - 1, app.hlpContact.GetContactCount());

            List<ContactData> newContacts = ContactData.GetAllContactsFromDB();

            oldContacts.RemoveAt(lIndexToRemove);  // Remove a contact from collection with random Id
            
            // We use here overloaded method "EQUALS", that is verifying every contact name in collection
            Assert.AreEqual(oldContacts, newContacts);
            foreach (ContactData contact in newContacts)
            {   // Verify that EXACT CONTACT with EXACT ID is deleted
                Assert.AreNotEqual(contact.Id, idOfContactToRemove);
            }
        }
    }
}