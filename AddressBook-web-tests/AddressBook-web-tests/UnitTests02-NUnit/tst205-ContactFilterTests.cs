﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Collections.Generic;
using NUnit.Framework;

namespace AddressBookWebTests
{
    [TestFixture]
    public class ContactFilterTestsNUNIT : ContactTestBase
    {
        [Test]
        public void Test_122_FilterContactListWithGoodResults()
        {
            string lSearchString = "380656451245";  // Use phone number in test

            int iCountFilteredCountacts = app.hlpContact.CountContactsContainingSearchString(lSearchString);
            app.hlpContact.SetTheSearchFilter(lSearchString);

            Assert.AreEqual(iCountFilteredCountacts, app.hlpContact.GetNumberOfSearchResults());
            Assert.AreEqual(iCountFilteredCountacts, app.hlpContact.GetNumberOfDisplayedRows());

            app.hlpContact.ResetTheSearchFilter();
        }

        [Test]
        public void Test_123_FilterContactListWithBadResults()
        {
            string lSearchString = "!!!!!!!!!!!!!";  // Use invalid string in test

            int iCountFilteredCountacts = app.hlpContact.CountContactsContainingSearchString(lSearchString);
            app.hlpContact.SetTheSearchFilter(lSearchString);

            Assert.AreEqual(iCountFilteredCountacts, app.hlpContact.GetNumberOfSearchResults());
            Assert.AreEqual(iCountFilteredCountacts, app.hlpContact.GetNumberOfDisplayedRows());

            app.hlpContact.ResetTheSearchFilter();
        }
    }
}
