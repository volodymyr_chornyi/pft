﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Collections.Generic;
using NUnit.Framework;

namespace AddressBookWebTests
{
    [TestFixture]
    public class GroupRenovalTestsNUNIT : GroupTestBase
    {
        [Test]
        public void Test_105_RemoveGroup()
        {
            EnsureAtLeastOneGroupPresent();
            List<GroupData> oldGroups = GroupData.GetAllGroupsFromDB();
            int lIndexToRemove = GenerateRandomInteger(0, oldGroups.Count - 1);
            string idOfGroupToRemove = oldGroups[lIndexToRemove].Id;

            app.hlpGroup.SelectGroup(idOfGroupToRemove) // Select a group in browser with exact Id
                .RemoveGroup(); // Remove selected group in browser
            
            // If tests often fail -- we verify groups list
            Assert.AreEqual(oldGroups.Count - 1, app.hlpGroup.GetGroupCount());

            List<GroupData> newGroups = GroupData.GetAllGroupsFromDB();

            oldGroups.RemoveAt(lIndexToRemove);  // Remove a group from collection with random Id
            // We use here overloaded method "EQUALS", that is verifying every group name in collection
            Assert.AreEqual(oldGroups, newGroups);
            foreach (GroupData group in newGroups)
            {   // Verify that EXACT GROUP with EXACT ID is deleted
                Assert.AreNotEqual(group.Id, idOfGroupToRemove);
            }
        }
    }
}
