﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;

namespace AddressBookWebTests
{
    public class TestBase
    {
        public static bool PERFORM_LONG_UI_CHECKS = true;
        protected ApplicationManager app;

        [SetUp]
        protected void SetupApplicationManager()
        {
            app = ApplicationManager.GetInstance();
        }

        public static Random rnd = new Random();

        public static string GenerateRandomString(int pMaxLength)
        {
            int lLength = Convert.ToInt32(rnd.NextDouble() * pMaxLength);
            StringBuilder lStringBuilder = new StringBuilder();
            for (int i = 0; i < lLength; i++)
            {
                lStringBuilder.Append(Convert.ToChar(32 + Convert.ToInt32(rnd.NextDouble() * 223)));
            }
            return lStringBuilder.ToString();
        }
        public static string GenerateRandomEnglishString(bool pFirstLetterUppercased, bool pOnlyLetters, int pMaxLength)
        {   // Random English string, first letter can be upercased
            int lLength = Convert.ToInt32(rnd.NextDouble() * pMaxLength);
            StringBuilder lStringBuilder = new StringBuilder();
            
            if (pFirstLetterUppercased)
            {
                var lCharsUppercased = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
                lStringBuilder.Append(lCharsUppercased[rnd.Next(lCharsUppercased.Length)]);
                if (lLength > 0)
                {
                    lLength--;
                }
            }
            var lCharsLowercased = "abcdefghijklmnopqrstuvwxyz";
            if (!pOnlyLetters)
            {
                lCharsLowercased += "0123456789 @#$%";
            }

            for (int i = 0; i < lLength; i++)
            {
                lStringBuilder.Append(lCharsLowercased[rnd.Next(lCharsLowercased.Length)]);
            }
            return lStringBuilder.ToString();
        }

        public static int GenerateRandomInteger(int pMinValue, int pMaxValue)
        {
            return Convert.ToInt32(rnd.NextDouble() * (pMaxValue - pMinValue)) + pMinValue;
        }
        public static T RandomEnumValue<T>()
        {
            return Enum
                .GetValues(typeof(T))
                .Cast<T>()
                .OrderBy(x => rnd.Next())
                .FirstOrDefault();
        }
        public enum eMonth
        {
            January = 0,
            February = 1,
            March = 2,
            April = 3,
            May = 4,
            June = 5,
            July = 6,
            August = 7,
            September = 8,
            October = 9,
            November = 10,
            December = 11
        }
        public enum eEmailDomains
        {
            de = 0,
            com = 1,
            net = 2,
            org = 3
        }
    }
}
