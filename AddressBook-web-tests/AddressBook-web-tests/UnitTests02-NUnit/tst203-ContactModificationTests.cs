﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Collections.Generic;
using NUnit.Framework;

namespace AddressBookWebTests
{
    [TestFixture]
    public class ContactModificationTestsNUNIT : ContactTestBase
    {
        [Test]
        public void Test_116_EditContactData()
        {
            ContactData lContact = new ContactData(GenerateRandomEnglishString(false, true, 40));
            lContact.ContFirstName = GenerateRandomEnglishString(true, true, 15);
            lContact.ContLastName = GenerateRandomEnglishString(true, true, 25);
            lContact.ContCompanyName = GenerateRandomEnglishString(true, true, 20);
            lContact.ContBirthDay = GenerateRandomInteger(1, 28).ToString();
            lContact.ContBirthMonth = RandomEnumValue<eMonth>().ToString(); // "February",
            lContact.ContBirthYear = GenerateRandomInteger(1900, DateTime.Now.Year - 1).ToString();  // Last possible year is previous

            EnsureAtLeastOneContactPresent();
            List<ContactData> oldContacts = ContactData.GetAllContactsFromDB();

            int lIndexToEdit = GenerateRandomInteger(0, oldContacts.Count - 1); // Random index to edit
            string idOfContactToEdit = oldContacts[lIndexToEdit].Id;

            oldContacts[lIndexToEdit].ContFirstName = lContact.ContFirstName;
            oldContacts[lIndexToEdit].ContLastName = lContact.ContLastName;
            oldContacts[lIndexToEdit].ContCompanyName = lContact.ContCompanyName;
            oldContacts[lIndexToEdit].ContBirthDay = lContact.ContBirthDay;
            oldContacts[lIndexToEdit].ContBirthMonth = lContact.ContBirthMonth;
            oldContacts[lIndexToEdit].ContBirthYear = lContact.ContBirthYear;

            app.hlpContact.EditContact(lIndexToEdit, lContact); // Edit the randomly chosen contact

            List<ContactData> newContacts = ContactData.GetAllContactsFromDB();
            oldContacts.Sort();
            newContacts.Sort();
            // We use here overloaded method "EQUALS", that is verifying every contact first name and last name in collection
            Assert.AreEqual(oldContacts, newContacts);
        }
    }
}
