﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;

namespace AddressBookWebTests
{
    public class GroupTestBase : AuthTestBase
    {
        [SetUp]
        protected void SetupGroupTest()
        {
            app.hlpNavigator.GoToGroupsPage();
        }

        protected void EnsureAtLeastOneGroupPresent()
        {
            // Create a temporary group when no other present
            if (!app.hlpGroup.AtLeastOneGroupPresent())
            {
                GroupData lTemporaryGroup = new GroupData("temporary group");
                lTemporaryGroup.GroupFooter = "temporary group footer";
                lTemporaryGroup.GroupHeader = "temporary group header";

                app.hlpGroup.CreateGroup(lTemporaryGroup);
                app.hlpNavigator.GoToGroupsPage();
            }
        }
        [TearDown]
        protected void CompareGroupsUI_DB()
        {
            if (PERFORM_LONG_UI_CHECKS)
            {
                List<GroupData> lFromUI = app.hlpGroup.GetAllGroupsFromUI();
                List<GroupData> lFromDB = GroupData.GetAllGroupsFromDB();
                lFromUI.Sort();
                lFromDB.Sort();
                Assert.AreEqual(lFromUI, lFromDB);
            }
        }
    }
}
