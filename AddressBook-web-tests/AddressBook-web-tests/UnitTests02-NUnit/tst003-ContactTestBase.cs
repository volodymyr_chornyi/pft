﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;

namespace AddressBookWebTests
{
    public class ContactTestBase : AuthTestBase
    {
        [SetUp]
        protected void SetupContactTest()
        {
            app.hlpNavigator.GoToContactsPage();
        }

        protected void EnsureAtLeastOneContactPresent()
        {
            if (!app.hlpContact.AtLeastOneContactPresent())
            {
                ContactData lContactData = new ContactData("temporary contact");
                lContactData.ContFirstName = "temporary FirstName";
                lContactData.ContLastName = "temporary LastName";
                lContactData.ContCompanyName = "temporary Company";
                lContactData.ContBirthDay = "1";
                lContactData.ContBirthMonth = "June";
                lContactData.ContBirthYear = "1985";

                app.hlpNavigator.GoToAddNewContactPage();
                app.hlpContact.CreateContact(lContactData);
                app.hlpNavigator.GoToContactsPage();
            }
        }
        public static string GenerateRandomPhoneNumber(int pPhoneLength)
        {
            string lPhone = "";
            for (int i = 0; i < pPhoneLength; i++)
            {
                lPhone += GenerateRandomInteger(0, 9).ToString();
            }
            return lPhone;
        }

        public static string GenerateRandomEmail()
        {
            return GenerateRandomEnglishString(true, true, 20)
                                    + "@" + GenerateRandomEnglishString(true, true, 10)
                                    + "." + RandomEnumValue<eEmailDomains>().ToString();
        }
        [TearDown]
        protected void CompareContactssUI_DB()
        {
            if (PERFORM_LONG_UI_CHECKS)
            {
                List<ContactData> lFromUI = app.hlpContact.GetAllContactsFromUI();
                List<ContactData> lFromDB = ContactData.GetAllContactsFromDB();
                lFromUI.Sort();
                lFromDB.Sort();
                Assert.AreEqual(lFromUI, lFromDB);
            }
        }
    }
}
