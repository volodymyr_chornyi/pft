﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Collections.Generic;
using NUnit.Framework;

namespace AddressBookWebTests
{
    [TestFixture]
    public class ContactInformationTestsNUNIT : ContactTestBase
    {
        [Test]
        public void Test_118_ContactInfoFromContactEditScreen()
        {
            int lTestContactIndex = 0;
            ContactData lInfoFromTable = app.hlpContact.GetContactInformationFromTable(lTestContactIndex);
            ContactData lInfoFromForm = app.hlpContact.GetContactInformationFromEditForm(lTestContactIndex);

            Assert.AreEqual(lInfoFromTable, lInfoFromForm);
            Assert.AreEqual(lInfoFromTable.ContAddress, lInfoFromForm.ContAddress);
            Assert.AreEqual(lInfoFromTable.ContAllEmails, lInfoFromForm.ContAllEmails);
            Assert.AreEqual(lInfoFromTable.ContAllPhones, lInfoFromForm.ContAllPhones);
        }
        [Test]
        public void Test_119_ContactInfoFromContactInfoScreen()
        {
            int lTestContactIndex = 0;
            ContactData lInfoFromForm = app.hlpContact.GetContactInformationFromEditForm(lTestContactIndex);
            ContactData lInfoFromInfoScreen = app.hlpContact.GetContactInformationFromInfoScreen(lTestContactIndex);

            Assert.AreEqual(lInfoFromInfoScreen.ContInfoDetailsWithoutLineBreaksAndSpaces, lInfoFromForm.ContInfoDetailsWithoutLineBreaksAndSpaces);
        }
    }
}
