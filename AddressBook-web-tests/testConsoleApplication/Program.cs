﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

// Using Console application for quick check in strings
namespace testConsoleApplication
{
    class Program
    {
        static void Main(string[] args)
        {
            var lContInfoRows = new List<string>();
            lContInfoRows.Add("TestCompany2");
            lContInfoRows.Add("");
            lContInfoRows.Add("email1@test1.com (www.test.com)");
            lContInfoRows.Add("email2@test2.com (www.test2.com)");
            lContInfoRows.Add("H: +380 (67) 9312963");
            lContInfoRows.Add("Ulj 55@ok6f78#0ukeh");
            lContInfoRows.Add("email@test.com (www.test.com)");
            lContInfoRows.Add("Noerxepnffkmfqczrbs@Mioyr.net (www.Mioyr.net)");
            lContInfoRows.Add("Uu@Qzerprq.com (www.Qzerprq.com)");
            lContInfoRows.Add("Birthday 15. March 1982 (32)");

            foreach (var lContInfoRow in lContInfoRows)
            {
                if (lContInfoRow.Contains("@"))
                {
                    var lDomainPattern = lContInfoRow.Split(new char[] { '@', ' ' })[1];
                    var lMatchesCount = Regex.Matches(lContInfoRow, lDomainPattern).Count;
                    if (lMatchesCount > 1)
                    {
                        Console.WriteLine(lContInfoRow);
                    }
                }
            }
            Console.Read();
        }
    }
}