﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using Newtonsoft.Json;
using Excel = Microsoft.Office.Interop.Excel;
using AddressBookWebTests;

namespace TestDataGenerators
{
    class DataGeneratorProgram
    {
        static void Main(string[] args)
        {
            string lDataType = args[0]; // "groups" , "contacts"
            int lCount = Convert.ToInt32(args[1]);  // Count of random data
            string lFileName = args[2];   // File name (groups.csv, contacts.xml, ...)
            string lFormat = args[3];   // excel / csv / xml / json

            if (lDataType == "groups")
            {
                List<GroupData> lGroups = new List<GroupData>();
                for (int i = 0; i < lCount; i++)
                {
                    lGroups.Add(new GroupData(TestBase.GenerateRandomEnglishString(false, false, 10))
                    {
                        GroupHeader = TestBase.GenerateRandomEnglishString(false, false, 100),
                        GroupFooter = TestBase.GenerateRandomEnglishString(false, false, 100)
                    });
                }

                if (lFormat == "excel")
                {
                    WriteGroupsToExcelFile(lGroups, lFileName);
                }
                else
                {   // Not EXCEL, write to file via Streamwriter
                    StreamWriter lGroupsWriter = new StreamWriter(lFileName);
                    if (lFormat == "csv")
                    {
                        WriteGroupsToCsvFile(lGroups, lGroupsWriter);
                    }
                    else if (lFormat == "xml")
                    {
                        WriteGroupsToXmlFile(lGroups, lGroupsWriter);
                    }
                    else if (lFormat == "json")
                    {
                        WriteGroupsToJsonFile(lGroups, lGroupsWriter);
                    }
                    else
                    {
                        Console.WriteLine("Unrecognized file format: " + lFormat);
                    }
                    lGroupsWriter.Close();
                }
            }
            else if (lDataType == "contacts")
            {
                List<ContactData> lContacts = new List<ContactData>();
                for (int i = 0; i < lCount; i++)
                {
                    lContacts.Add(new ContactData(TestBase.GenerateRandomEnglishString(true, true, 20))
                    {
                        ContFirstName = TestBase.GenerateRandomEnglishString(true, false, 40),
                        ContLastName = TestBase.GenerateRandomEnglishString(true, false, 40),
                        ContCompanyName = TestBase.GenerateRandomEnglishString(true, false, 40),
                        ContBirthDay = TestBase.GenerateRandomInteger(1, 31).ToString(),
                        ContBirthMonth = TestBase.RandomEnumValue<TestBase.eMonth>().ToString(), // "February", "March", etc.
                        ContBirthYear = TestBase.GenerateRandomInteger(1900, DateTime.Now.Year - 1).ToString(),  // last possible year is previous

                        ContHomePhone = "+380" + ContactTestBase.GenerateRandomPhoneNumber(9),
                        ContMobilePhone = "+380" + ContactTestBase.GenerateRandomPhoneNumber(9),

                        ContEmail = ContactTestBase.GenerateRandomEmail(),
                        ContEmail3 = ContactTestBase.GenerateRandomEmail(),

                        ContAddress = TestBase.GenerateRandomEnglishString(true, false, 20) + "\r\n"
                                        + TestBase.GenerateRandomEnglishString(true, false, 20) + "\r\n"
                                        + TestBase.GenerateRandomEnglishString(true, false, 20)
                    });
                    // Correct birth date if outside of possible ones
                    if (lContacts[i].ContBirthMonth == "February")
                    {
                        if (Convert.ToInt32(lContacts[i].ContBirthDay) > 28)
                        {
                            lContacts[i].ContBirthDay = "28";
                        }
                    }
                    else if (lContacts[i].ContBirthMonth == "April"
                        || lContacts[i].ContBirthMonth == "June"
                        || lContacts[i].ContBirthMonth == "September"
                        || lContacts[i].ContBirthMonth == "November"
                    )
                    {
                        if (lContacts[i].ContBirthDay == "31")
                        {
                            lContacts[i].ContBirthDay = "30";
                        }
                    }
                }
                StreamWriter lContactsWriter = new StreamWriter(lFileName);   // File name (groups.csv, contacts.xml, ...)
                if (lFormat == "csv")
                {
                    WriteContactsToCsvFile(lContacts, lContactsWriter);
                }
                else if (lFormat == "xml")
                {
                    WriteContactsToXmlFile(lContacts, lContactsWriter);
                }
                else if (lFormat == "json")
                {
                    WriteContactsToJsonFile(lContacts, lContactsWriter);
                }
                else
                {
                    Console.WriteLine("Unrecognized file format: " + lFormat);
                }
                lContactsWriter.Close();
            }
            else
            {
                Console.WriteLine("Unrecognized data type: " + lDataType);
            }

        }

        static void WriteGroupsToExcelFile(List<GroupData> pGroups, string pFileName)
        {
            Excel.Application lExcelApp = new Excel.Application();
            Excel.Workbook lWorkBook = lExcelApp.Workbooks.Add();
            Excel.Worksheet lSheet = lWorkBook.ActiveSheet;

            int lRow = 1;
            foreach (GroupData lGroup in pGroups)
            {
                lSheet.Cells[lRow, 1] = lGroup.GroupName;
                lSheet.Cells[lRow, 2] = lGroup.GroupHeader;
                lSheet.Cells[lRow, 3] = lGroup.GroupFooter;
                lRow++;
            }
            string lFullPath = Path.Combine(Directory.GetCurrentDirectory(), pFileName);
            File.Delete(lFullPath);
            lWorkBook.SaveAs(lFullPath);
            lWorkBook.Close();
            lExcelApp.Quit();
        }

        ///////////////////////
        static void WriteGroupsToCsvFile(List<GroupData> pGroups, StreamWriter pWriter)
        {
            foreach (GroupData lGroup in pGroups)
            {
                pWriter.WriteLine(String.Format("${0},${1},${2}",
                    lGroup.GroupName, lGroup.GroupHeader, lGroup.GroupFooter
                    ));
            }
        }
        static void WriteGroupsToXmlFile(List<GroupData> pGroups, StreamWriter pWriter)
        {
            new XmlSerializer(typeof(List<GroupData>)).Serialize(pWriter, pGroups);
        }
        static void WriteGroupsToJsonFile(List<GroupData> pGroups, StreamWriter pWriter)
        {
            pWriter.Write(JsonConvert.SerializeObject(pGroups,
                Newtonsoft.Json.Formatting.Indented));
        }
        ///////////////////////
        static void WriteContactsToCsvFile(List<ContactData> pContacts, StreamWriter pWriter)
        {
            foreach (ContactData lContact in pContacts)
            {
                // We replace line breaks in the address with "xxxxxxxxxxxxxxx" to have possiblity to be able to read data in one line
                pWriter.WriteLine(String.Format(@"{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11}",
                    lContact.ContName, lContact.ContFirstName, lContact.ContLastName,
                    lContact.ContCompanyName, lContact.ContBirthDay, lContact.ContBirthMonth,
                    lContact.ContBirthYear, lContact.ContHomePhone, lContact.ContMobilePhone,
                    lContact.ContEmail, lContact.ContEmail3, lContact.ContAddress.Replace("\r\n", "xxxxxxxxxxxxxxx")
                    ));
            }
        }
        static void WriteContactsToXmlFile(List<ContactData> pContacts, StreamWriter pWriter)
        {
            new XmlSerializer(typeof(List<ContactData>)).Serialize(pWriter, pContacts);
        }
        static void WriteContactsToJsonFile(List<ContactData> pContacts, StreamWriter pWriter)
        {
            pWriter.Write(JsonConvert.SerializeObject(pContacts,
                Newtonsoft.Json.Formatting.Indented));
        }
    }
}