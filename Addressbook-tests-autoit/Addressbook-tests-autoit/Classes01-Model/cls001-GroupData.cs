﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Addressbook_tests_autoit
{
    public class GroupData : IComparable<GroupData>, IEquatable<GroupData>
    {
        public GroupData()
        {
        }
        public GroupData(string pName)
        {
            GroupName = pName;
        }

        public string GroupName { get; set; }

        public int CompareTo(GroupData other)
        {
            return this.GroupName.CompareTo(other.GroupName);
        }

        public bool Equals(GroupData other)
        {
            return this.GroupName.Equals(other.GroupName);
        }
    }
}
