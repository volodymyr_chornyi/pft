﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace Addressbook_tests_autoit
{
    public class TestBase
    {
        public ApplicationManager app;

        [TestFixtureSetUp]
        public void initApplication()
        {
            app = new ApplicationManager();
        }

        [TestFixtureTearDown]
        public void stopApplication()
        {
            app.Stop();
        }

        public static Random rnd = new Random();

        public static string GenerateRandomString(int pMaxLength)
        {
            int lLength = Convert.ToInt32(rnd.NextDouble() * pMaxLength);
            StringBuilder lStringBuilder = new StringBuilder();
            for (int i = 0; i < lLength; i++)
            {
                lStringBuilder.Append(Convert.ToChar(32 + Convert.ToInt32(rnd.NextDouble() * 223)));
            }
            return lStringBuilder.ToString();
        }

        public static string GenerateRandomEnglishString(bool pFirstLetterUppercased, bool pOnlyLetters, int pMaxLength)
        {   // Random English string, first letter can be upercased
            int lLength = Convert.ToInt32(rnd.NextDouble() * pMaxLength);
            StringBuilder lStringBuilder = new StringBuilder();

            if (pFirstLetterUppercased)
            {
                var lCharsUppercased = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
                lStringBuilder.Append(lCharsUppercased[rnd.Next(lCharsUppercased.Length)]);
                if (lLength > 0)
                {
                    lLength--;
                }
            }
            var lCharsLowercased = "abcdefghijklmnopqrstuvwxyz";
            if (!pOnlyLetters)
            {
                lCharsLowercased += "0123456789 @#$%";
            }

            for (int i = 0; i < lLength; i++)
            {
                lStringBuilder.Append(lCharsLowercased[rnd.Next(lCharsLowercased.Length)]);
            }
            return lStringBuilder.ToString();
        }
        public static int GenerateRandomInteger(int pMinValue, int pMaxValue)
        {
            return Convert.ToInt32(rnd.NextDouble() * (pMaxValue - pMinValue)) + pMinValue;
        }
    }
}
