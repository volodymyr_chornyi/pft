﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;

namespace Addressbook_tests_autoit
{
    public class GroupTestBase : TestBase
    {
        [SetUp]
        protected void SetupGroupTest()
        {
            app.hlpGroup.OpenGroupsDialogue();
        }

        [TearDown]
        protected void FinishGroupTest()
        {
            app.hlpGroup.CloseGroupsDialogue();
        }

        protected void EnsureAtLeastTwoGroupsPresent()
        {
            // Create a temporary group when ONLY ONE present (last group cannot be deleted)
            if (app.hlpGroup.GetGroupsCount() == 1)
            {
                GroupData lTemporaryGroup = new GroupData("temporary group");

                app.hlpGroup.CreateGroup(lTemporaryGroup);
                //app.hlpGroup.OpenGroupsDialogue();
            }
        }
    }
}