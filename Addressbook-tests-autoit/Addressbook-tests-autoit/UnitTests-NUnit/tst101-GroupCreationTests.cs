﻿using System;
using System.Collections.Generic;
using NUnit.Framework;

namespace Addressbook_tests_autoit
{
    [TestFixture]
    public class GroupCreationTests : GroupTestBase
    {
        public GroupData RandomGroupDataGenerator()
        {
            GroupData lGroup = new GroupData();
            lGroup.GroupName = GenerateRandomEnglishString(true, true, 25);
            return lGroup;
        }

        [Test]
        public void Test_101_CreateRandomGroup()
        {
            List<GroupData> oldGroups = app.hlpGroup.GetGroupsList();
            GroupData newGroup = RandomGroupDataGenerator();

            app.hlpGroup.CreateGroup(newGroup);

            List<GroupData> newGroups = app.hlpGroup.GetGroupsList();
            oldGroups.Add(newGroup);
            oldGroups.Sort();
            newGroups.Sort();
            Assert.AreEqual(oldGroups, newGroups);
        }
    }
}
