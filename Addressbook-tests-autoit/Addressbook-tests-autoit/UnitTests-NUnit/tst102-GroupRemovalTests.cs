﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Collections.Generic;
using NUnit.Framework;

namespace Addressbook_tests_autoit
{
    [TestFixture]
    public class GroupRenovalTests : GroupTestBase
    {
        [Test]
        public void Test_105_RemoveGroup()
        {
            EnsureAtLeastTwoGroupsPresent();
            List<GroupData> oldGroups = app.hlpGroup.GetGroupsList();
            int lIndexToRemove = GenerateRandomInteger(0, oldGroups.Count - 1); // We set the random group to be removed
            oldGroups.Sort();

            app.hlpGroup.SelectGroup(lIndexToRemove) // Select a group
                .RemoveGroup(); // Click to remove and confirm deletion

            List<GroupData> newGroups = app.hlpGroup.GetGroupsList();

            oldGroups.RemoveAt(lIndexToRemove);  // Remove a group from collection with random Id
            newGroups.Sort();
            Assert.AreEqual(oldGroups, newGroups);
        }
    }
}
