﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoItX3Lib;

namespace Addressbook_tests_autoit
{
    public class HelperBase
    {
        protected string mWINTITLE;
        protected ApplicationManager mManager;
        protected AutoItX3 mAux;

        public HelperBase (ApplicationManager pManager)
        {
            this.mManager = pManager;
            this.mAux = mManager.Aux;
            mWINTITLE = ApplicationManager.WINTITLE;
        }
    }
}
