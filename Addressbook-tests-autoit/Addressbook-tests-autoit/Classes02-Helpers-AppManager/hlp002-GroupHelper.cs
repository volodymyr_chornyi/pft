﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Addressbook_tests_autoit
{
    public class GroupHelper : HelperBase
    {
        public static string GROUPWINTITLE = "Group editor";
        public static string DELETEGROUPWINTITLE = "Delete group";
        public static string GROUPCONTROLTREE = "WindowsForms10.SysTreeView32.app.0.afceb1";


        public GroupHelper(ApplicationManager manager)
            : base(manager)
        {

        }

        public List<GroupData> GetGroupsList()
        {
            List<GroupData> lAllGroupsList = new List<GroupData>();
            //OpenGroupsDialogue();

            int lCount = GetGroupsCount();
            
            for (int i = 0; i<lCount; i++)
            {
                string lItem = mAux.ControlTreeView(GROUPWINTITLE, "", "WindowsForms10.SysTreeView32.app.0.afceb1",
                "GetText", "#0|#"+i, "");
                System.Console.Out.WriteLine("I = " + i + " lItem = " + lItem);
                lAllGroupsList.Add(new GroupData ()
                {
                    GroupName = lItem
                });
            }
            //CloseGroupsDialogue();
            return lAllGroupsList;
        }

        public int GetGroupsCount()
        {
            string lCount = mAux.ControlTreeView(GROUPWINTITLE, "", "WindowsForms10.SysTreeView32.app.0.afceb1",
                "GetItemCount", "#0", "");
            return int.Parse(lCount);
        }

        public void CreateGroup(GroupData newGroup)
        {
            mAux.ControlClick(GROUPWINTITLE, "", GROUPCONTROLTREE); // 'New' button
            mAux.Send(newGroup.GroupName);
            mAux.Send("{ENTER}"); // Press the ENTER key
            // Close and open Groups dialogue, so the content is refreshed
            CloseGroupsDialogue();
            OpenGroupsDialogue();
        }

        public void OpenGroupsDialogue()
        {
            mAux.ControlClick(mWINTITLE, "", "WindowsForms10.BUTTON.app.0.afceb12"); // 'Edit groups' button
            mAux.WinWait(GROUPWINTITLE);
        }
        public void CloseGroupsDialogue()
        {
            mAux.ControlClick(GROUPWINTITLE, "", "WindowsForms10.BUTTON.app.0.afceb4"); // 'Close' button
        }

        public GroupHelper SelectGroup(int pIndexToRemove)
        {
            mAux.ControlTreeView(GROUPWINTITLE, "", GROUPCONTROLTREE,
                "Select", "#0|#" + pIndexToRemove, "");
            return this;
        }

        public GroupHelper RemoveGroup()
        {
            mAux.ControlClick(GROUPWINTITLE, "", "WindowsForms10.BUTTON.app.0.afceb1"); // 'Delete' button
            mAux.WinWait(DELETEGROUPWINTITLE);
            mAux.ControlClick(DELETEGROUPWINTITLE, "", "WindowsForms10.BUTTON.app.0.afceb1"); // 'Delete the selected group' radio option
            mAux.ControlClick(DELETEGROUPWINTITLE, "", "WindowsForms10.BUTTON.app.0.afceb3"); // Confirm deletion using 'OK' button
            // Close and open Groups dialogue, so the content is refreshed
            CloseGroupsDialogue();
            OpenGroupsDialogue();
            return this;
        }
    }
}