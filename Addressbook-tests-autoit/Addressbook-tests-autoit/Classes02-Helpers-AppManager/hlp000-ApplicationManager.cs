﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoItX3Lib;

namespace Addressbook_tests_autoit
{
    public class ApplicationManager
    {
        public static string WINTITLE = "Free Address Book";
        private AutoItX3 aux;
        protected GroupHelper helperGroups;
        public ApplicationManager()
        {
            aux = new AutoItX3();
            aux.Run(@"C:\Program Files (x86)\GAS Softwares\Free Address Book\AddressBook.exe",
                "", aux.SW_SHOW);
            AuxWinWaitAndActivate();
            helperGroups = new GroupHelper(this);
        }

        private void AuxWinWaitAndActivate()
        {
            aux.WinWait(WINTITLE);
            aux.WinActivate(WINTITLE);
            aux.WinWaitActive(WINTITLE);
        }

        public void Stop()
        {
            aux.ControlClick(WINTITLE, "", "WindowsForms10.RichEdit20W.app.0.62e4491");
        }

        public AutoItX3 Aux
        {
            get
            {
                return aux;
            }
        }

        public GroupHelper hlpGroup
        {
            get
            {
                return helperGroups;
            }
        }
    }
}
